.PHONY: test

test:
	python manage.py test --settings=microblog.settings.testing

coverage:
	coverage run manage.py test --settings=microblog.settings.testing
	coverage report

server:
	python manage.py runserver 0.0.0.0:8000
