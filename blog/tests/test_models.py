from django.test import TestCase
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.core.urlresolvers import reverse
from ..models import Post


class BlogTests(TestCase):

    def setUp(self):
        self.user = User.objects.create()

    def create_post(self, title="Test Blog Post", published=True):
	return Post.objects.create(title=title, author=self.user, published=published)

    def test_model_creation(self):
        post = self.create_post() 
	assert isinstance(post, Post), True
	assert str(post), post.title
	assert post.slug, slugify(post.title)

    def test_model_url(self):
	post = self.create_post()
	assert post.get_absolute_url, reverse('blog:detail', kwargs={'slug': post.slug})

