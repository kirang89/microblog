from django.conf.urls import patterns, include, url
from .views import PostListView, PostDetailView


urlpatterns = patterns('',
    url(r"^list/$", PostListView.as_view(), name="list"),
    url(r"^detail/(?P<slug>[\w-]+)/$", PostDetailView.as_view(), name="detail"),
)
